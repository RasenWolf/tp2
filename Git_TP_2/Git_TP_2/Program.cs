﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Git_TP_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence, phrase_tmp, Tab1_as_string, test_as_string;
            int[] Tab1 = new int[10];
            int compteur, test, Var;
            char pre_car_Maj, pre_car, der_car, car_point;

            Console.Write("Saisissez une phrase :\n");
            sentence = Console.ReadLine();
            Console.Write("Vous avez saisi :\n");
            Console.WriteLine(sentence);
            car_point = '.';


            if (Char.IsUpper(sentence[0]) == true)
            {
                Console.Write("Cette phrase commence par une majuscule.\n");
            }
            else
            {
                Console.Write("Cette phrase ne commence pas par une majuscule.\n");
            }

            pre_car_Maj = char.ToUpper(sentence[0]);
            pre_car = sentence[0];

            if (sentence[sentence.Length - 1] == '.')
            {
                Console.Write("Cette phrase se termine par un point.\n");
            }
            else
            {
                Console.Write("Cette phrase ne se termine pas par un point.\n");
            }

            der_car = sentence[sentence.Length - 1];
            

            Console.WriteLine("Saisissez dix valeurs :");
            for (compteur = 0; compteur < 10; compteur++)
            {
                phrase_tmp = String.Format("{0}: ", compteur + 1);
                Console.Write(phrase_tmp);
                Tab1_as_string = Console.ReadLine();
                int.TryParse(Tab1_as_string, out Tab1[compteur]);
            }
            Console.WriteLine("Saisissez une valeur :");
            test_as_string = Console.ReadLine();
            int.TryParse(test_as_string, out test);
            for (compteur = 0; compteur < 10; compteur++)
            {
                if (Tab1[compteur] == test)
                {
                    phrase_tmp = String.Format("{0} est la {1}-eme valeur saisie", test, compteur + 1);
                    Console.WriteLine(phrase_tmp);
                    break;
                }
            }
            Var = Tab1[compteur];
        }

        public static char Majuscule(char pre_car, char pre_car_Maj)
        {
            return pre_car_Maj = pre_car;
        }

        public static char Point(char der_car, char car_point)
        {
            return der_car = car_point;
        }

        public static int ValeurID(int test, int Var)
        {
            return test = Var;
        }
    }
}
