﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class Testout
    {
        private object b;
        private object a;

        public object A { get => a; set => a = value; }
        public object B { get => b; set => b = value; }

        [TestMethod]
        public void Majuscule()
        {
            Assert.AreEqual(A, B);
        }

        [TestMethod]
        public void Point()
        {
            Assert.AreEqual(A, B);
        }

        [TestMethod]
        public void ValeurID()
        {
            Assert.AreEqual(A,B);
        }
        
    }
    
}
